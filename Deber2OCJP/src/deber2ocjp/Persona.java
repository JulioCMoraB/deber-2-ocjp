/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deber2ocjp;

/**
 *
 * @author julio_000
 */
public class Persona implements Cantante{
    public String nombre;
    public int edad;
    public Computadora comp1;
 
    public Persona(){
    
    }
    //sobrecarga de constructores
   
    public Persona(String nombre) {
        this.nombre = nombre;
    }
    public Persona(int edad) {
        this.edad = edad;
    }
 
    public Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    @Override
    public void cantar() {
        System.out.println("Los pollitos dicen pio pio ");
    }
    
    public int OperacionAritmetica(int i,int j){
        return i*j;
    }
    
    public int OperacionAritmetica(int i,int j,int k){
        return (i+j)/k;
    }
    public void actuar(){
        System.out.println("La persona actua");
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", edad=" + edad + '}';
    }
    public void usaComputadora(Computadora comp1){
        System.out.println("La persona usa una computadora"+comp1.toString());
    }
    
           
}
