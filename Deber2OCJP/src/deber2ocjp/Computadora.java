/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deber2ocjp;

/**
 *
 * @author julio_000
 */
public class Computadora {
    private String marca;
    private String modelo;
    private String numserie;
    private int anios_uso;

    public Computadora(String marca, String modelo, String numserie, int anios_uso) {
        this.marca = marca;
        this.modelo = modelo;
        this.numserie = numserie;
        this.anios_uso = anios_uso;
    }

    @Override
    public String toString() {
        return "Computadora{" + "marca=" + marca + ", modelo=" + modelo + ", numserie=" + numserie + ", anios_uso=" + anios_uso + '}';
    }
    
    
}
